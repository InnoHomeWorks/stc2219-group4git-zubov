package homework05;
/*
Реализовать класс со статическими методами:

* Сложение всех чисел, которые передаются в метод. Метод возвращает результат сложения.

* Вычитание всех чисел, которые передаются в метод. Метод возвращает результат вычитания.
	* Доп. задание: найти наибольшее число из чисел, которые передали в метод. И производить вычитание из него.

* Умножение всех чисел, которые передаются в метод. Метод возвращает результат умножения.

* Деление всех чисел, которые передаются в метод. При каждом следующем делении должна идти проверка:
	* что делимое число должно быть больше делителя. Если условие не выполнено, то метод возвращает текущий результат деления.
	* что делитель - положительное число. Если условие не выполнено, то метод возвращает текущий результат деления.

* Метод, высчитывающий факториал переданного числа. Должна быть проверка, что переданное число положительное.
 */

import java.math.BigInteger;
import java.util.Arrays;

public class StaticMethods {

    public static void main(String[] args) {

        // version 1 - static nested class with static methods
        // invokes math methods
        System.out.println(StaticMathOperations.addition(0,2,3,5,6,8));
        System.out.println(StaticMathOperations.subtraction(0,10,5,4,1,5));
        System.out.println(StaticMathOperations.subtractionFromMax(3,15,5,4,1,5,7));
        System.out.println(StaticMathOperations.multiplication(3,15,5,4,1,5,7));
        System.out.println(StaticMathOperations.division(-4,300,-15, 5, 200, 0, 2));
        System.out.println(Arrays.toString(StaticMathOperations.factorial(-5,2,10,5,0,20)));
        System.out.println();

        // version 2 - nonstatic class (with common args) including static methods
        // invokes example of MathOperations with args and math methods
        System.out.println(new MathOperations(20,5,25,-5,-3,10,2,1));
        System.out.println(MathOperations.addition());
        System.out.println(MathOperations.subtraction());
        System.out.println(MathOperations.subtractionFromMax());
        System.out.println(MathOperations.multiplication());
        System.out.println(MathOperations.division());
        System.out.println(Arrays.toString(MathOperations.factorial()));

    }

    private static class StaticMathOperations {

        private static int addition(int... numbers) {

            System.out.print("Сложение чисел: " + Arrays.toString(numbers) + " = ");

            int result = 0;

            for (int j : numbers) {

                result += j;
            }

            return result;
        }

        private static int subtraction(int... numbers) {

            System.out.print("Вычитание чисел: " + Arrays.toString(numbers) + " = ");

            int result = numbers[0];

            for (int i = 1; i < numbers.length; i++) {

                result -= numbers[i];
            }

            return result;
        }

        private static int subtractionFromMax(int... numbers) {

            System.out.print("Вычитание чисел из максимума: " + Arrays.toString(numbers) + " = ");

            int max = numbers[0];

            // find max of numbers
            for (int i = 1; i < numbers.length; i++) {

                if (max < numbers [i]) {

                    max = numbers [i];
                }
            }

            int result = max;

            for (int j : numbers) {

                // skip subtraction
                if (j == max) {
                    continue;
                }

                result -= j;
            }

            return result;
        }

        private static int multiplication(int... numbers) {

            System.out.print("Умножение чисел: " + Arrays.toString(numbers) + " = ");

            int result = numbers[0];

            for (int i = 1; i < numbers.length; i++) {

                result *= numbers[i];
            }

            return result;
        }

        private static int division(int... numbers) {

            System.out.print("Деление чисел: " + Arrays.toString(numbers) + " = ");

            int result = 0;
            boolean ifDividendFound = false;

            for (int i = 0; i < numbers.length-1; i++) {

                // find dividend for division
                if(numbers[i] > 0 && !ifDividendFound) {

                    result = numbers[i];
                    ifDividendFound = true;
                }

                if (result > numbers[i+1] && numbers[i+1] > 0) {

                    result /= numbers[i+1];
                }
            }

            return result;
        }

        private static BigInteger[] factorial(int... numbers) {

            System.out.print("Факториал чисел (по отдельности): " + Arrays.toString(numbers) + " = ");

            // create BigInteger array for big value of factorial result
            BigInteger[] result = new BigInteger[numbers.length];

            // loop choose each number of array
            for (int j = 0; j < numbers.length; j++) {

                if (numbers[j] >= 0) {

                    BigInteger bigNumber = BigInteger.ONE;

                    // loop for calculation factorial of each number of array
                    for (int i = 1; i <= numbers[j]; i++) {

                        bigNumber = bigNumber.multiply(BigInteger.valueOf(i));
                    }

                    result[j] = bigNumber;

                } else result[j] = BigInteger.ZERO;
            }

            return result;
        }
    }
}