package homework05;

import java.math.BigInteger;
import java.util.Arrays;

public class MathOperations {

    private static int[] numbers;

    public MathOperations(int ... values) {

        // encapsulate elements of array numbers
        numbers = Arrays.copyOf(values, values.length);
    }

    static int addition() {

        System.out.print("Сложение чисел: ");

        int result = 0;

        for (int j : numbers) {

            result += j;
        }

        return result;
    }

    static int subtraction() {

        System.out.print("Вычитание чисел: ");

        int result = numbers[0];

        for (int i = 1; i < numbers.length; i++) {

            result -= numbers[i];
        }

        return result;
    }

    static int subtractionFromMax() {

        System.out.print("Вычитание чисел из максимума: ");

        int max = numbers[0];

        // find max of numbers
        for (int i = 1; i < numbers.length; i++) {

            if (max < numbers[i]) {

                max = numbers[i];
            }
        }

        int result = max;

        for (int j : numbers) {

            // skip subtraction
            if (j == max) {
                continue;
            }

            result -= j;
        }

        return result;
    }

    static int multiplication() {

        System.out.print("Умножение чисел: ");


        int result = numbers[0];

        for (int i = 1; i < numbers.length; i++) {

            result *= numbers[i];
        }

        return result;
    }

    static int division() {

        System.out.print("Деление чисел: ");

        int result = 0;
        boolean ifDividendFound = false;

        for (int i = 0; i < numbers.length-1; i++) {

            // find dividend for division
            if(numbers[i] > 0 && !ifDividendFound) {

                result = numbers[i];
                ifDividendFound = true;
            }

            if (result > numbers[i+1] && numbers[i+1] > 0) {

                result /= numbers[i+1];
            }
        }

        return result;
    }

    static BigInteger[] factorial() {

        System.out.print("Факториал чисел (по отдельности): ");

        // create BigInteger array for big value of factorial result
        BigInteger[] result = new BigInteger[numbers.length];

        // loop choose each number of array
        for (int j = 0; j < numbers.length; j++) {

            if (numbers[j] >= 0) {

                BigInteger bigNumber = BigInteger.ONE;

                // loop for calculation factorial of each number of array
                for (int i = 1; i <= numbers[j]; i++) {

                    bigNumber = bigNumber.multiply(BigInteger.valueOf(i));
                }

                result[j] = bigNumber;

            } else result[j] = BigInteger.ZERO;
        }

        return result;
    }

    @Override
    public String toString(){

        return "Операции над числами " + Arrays.toString(numbers);
    }
}
