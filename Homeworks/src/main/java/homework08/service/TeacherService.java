package homework08.service;

import homework08.dao.TeacherDAO;
import homework08.model.StudentEntity;
import homework08.model.TeacherEntity;
import jakarta.persistence.EntityManager;

import java.io.Serializable;
import java.util.List;

public class TeacherService {

    private EntityManager em;

    public TeacherService(EntityManager em) {
        this.em = em;
    }

    public void printTeacher(Long id) {

        TeacherDAO teacherDAO = new TeacherDAO(em);
        TeacherEntity teacher = teacherDAO.findTeacher(id);

        System.out.printf("У преподавателя %d : %s %s следующие студенты: ",
                teacher.getId(), teacher.getName(), teacher.getLastName());
        System.out.println();
        System.out.println("Первый вариант:");
        List<StudentEntity> studentsOfTeacher1 = teacherDAO.findStudentsOfTeacher1(id);
        System.out.println(studentsOfTeacher1);

        System.out.println("Второй вариант:");
        List<List<? extends Serializable>> studentsOfTeacher2 = teacherDAO.findStudentsOfTeacher2(id);
        System.out.println(studentsOfTeacher2);
    }
}