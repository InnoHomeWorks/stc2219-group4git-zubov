package homework08.service;

import homework08.dao.StudentDAO;
import homework08.model.StudentEntity;
import homework08.model.TeacherEntity;
import jakarta.persistence.EntityManager;

import java.io.Serializable;
import java.util.List;

public class StudentService {

    private EntityManager em;

    public StudentService(EntityManager em) {
        this.em = em;
    }

    public void printStudent(Long id) {

        StudentDAO studentDAO = new StudentDAO(em);
        StudentEntity student = studentDAO.findStudent(id);

        System.out.printf("У студента %d : %s %s следующие преподаватели: ",
                student.getId(), student.getName(), student.getLastName());
        System.out.println();
        System.out.println("Первый вариант:");
        List<TeacherEntity> teachersOfStudent1 = studentDAO.findTeachersOfStudent1(id);
        System.out.println(teachersOfStudent1);

        System.out.println("Второй вариант:");
        List<List<? extends Serializable>> teachersOfStudent2 = studentDAO.findTeachersOfStudent2(id);
        System.out.println(teachersOfStudent2);
    }
}
