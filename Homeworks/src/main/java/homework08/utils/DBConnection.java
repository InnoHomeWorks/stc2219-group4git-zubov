package homework08.utils;

import homework08.model.StudentEntity;
import homework08.model.TeacherEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import org.hibernate.cfg.Configuration;

public class DBConnection {

    public EntityManager getEm() {

        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(TeacherEntity.class)
                .addAnnotatedClass(StudentEntity.class)
                .buildSessionFactory();

        return factory.createEntityManager();
    }

}
