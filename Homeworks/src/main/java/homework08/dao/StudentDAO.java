package homework08.dao;

import homework08.model.StudentEntity;
import homework08.model.TeacherEntity;
import jakarta.persistence.EntityManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentDAO {

    private EntityManager em;

    public StudentDAO(EntityManager em) {
        this.em = em;
    }

    public StudentEntity findStudent(Long id) {

        return em.find(StudentEntity.class, id);
    }

    public List<TeacherEntity> findTeachersOfStudent1(Long id) {

        List<TeacherEntity> teachers = new ArrayList<>();

        em.createQuery("select s.teachers from StudentEntity s where s.id = " + id, TeacherEntity.class)
                .getResultList()
                .forEach(p -> {
                    TeacherEntity teacher = new TeacherEntity();
                    teacher.setId(p.getId());
                    teacher.setName(p.getName());
                    teacher.setLastName(p.getLastName());
                    teachers.add(teacher);
                });
        return teachers;
    }

    public List<List<? extends Serializable>> findTeachersOfStudent2(Long id) {

        return em.createQuery("select s.teachers from StudentEntity s where s.id = " + id, TeacherEntity.class)
                .getResultList()
                .stream()
                .map(p -> List.of(p.getId(), p.getName(), p.getLastName()))
                .collect(Collectors.toList());
    }
}