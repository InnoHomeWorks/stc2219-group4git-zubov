package homework08.dao;

import homework08.model.StudentEntity;
import homework08.model.TeacherEntity;
import jakarta.persistence.EntityManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TeacherDAO {

    private EntityManager em;

    public TeacherDAO(EntityManager em) {
        this.em = em;
    }

    public TeacherEntity findTeacher(Long id) {

        return em.find(TeacherEntity.class, id);
    }

    public List<StudentEntity> findStudentsOfTeacher1(Long id) {

        List<StudentEntity> students = new ArrayList<>();

        em.createQuery("select t.students from TeacherEntity t where t.id = " + id, StudentEntity.class)
                .getResultList()
                .forEach(p -> {
                    StudentEntity student = new StudentEntity();
                    student.setId(p.getId());
                    student.setName(p.getName());
                    student.setLastName(p.getLastName());
                    students.add(student);
                });
        return students;
    }

    public List<List<? extends Serializable>> findStudentsOfTeacher2(Long id) {

        return em.createQuery("select t.students from TeacherEntity t where t.id = " + id, StudentEntity.class)
                .getResultList()
                .stream()
                .map(p -> List.of(p.getId(), p.getName(), p.getLastName()))
                .collect(Collectors.toList());
    }
}
