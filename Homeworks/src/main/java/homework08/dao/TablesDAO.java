package homework08.dao;

import homework08.model.StudentEntity;
import homework08.model.TeacherEntity;
import jakarta.persistence.EntityManager;

import java.util.List;

public class TablesDAO {

    public void createExample(EntityManager em) {

        em.getTransaction().begin();

        TeacherEntity teacher1 = new TeacherEntity();
        TeacherEntity teacher2 = new TeacherEntity();
        TeacherEntity teacher3 = new TeacherEntity();

        StudentEntity student1 = new StudentEntity();
        StudentEntity student2 = new StudentEntity();
        StudentEntity student3 = new StudentEntity();

        teacher1.setName("Charles");
        teacher1.setLastName("Darwin");
        teacher1.setStudents(List.of(student1));

        teacher2.setName("Michael");
        teacher2.setLastName("Faraday");
        teacher2.setStudents(List.of(student2, student3));

        teacher3.setName("Galileo");
        teacher3.setLastName("Galilei");
        teacher3.setStudents(List.of(student3));

        student1.setName("Yuri");
        student1.setLastName("Barankin");
        student1.setTeachers(List.of(teacher3));

        student2.setName("Konstantin");
        student2.setLastName("Malinin");
        student2.setTeachers(List.of(teacher2, teacher3));

        student3.setName("Zoya");
        student3.setLastName("Fokina");
        student3.setTeachers(List.of(teacher3));

        em.persist(teacher1);
        em.persist(teacher2);
        em.persist(teacher3);
        em.persist(student1);
        em.persist(student2);
        em.persist(student3);

        em.getTransaction().commit();
    }
}
