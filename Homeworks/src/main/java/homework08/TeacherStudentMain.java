package homework08;

/*
8. Hibernate
Реализовать связь **ManyToMany** (Многие-Ко-Многим) на примере - Преподаватель и Студент.
У одного студента может быть несколько преподавателей, а у одного преподавателя - несколько студентов.
В консоль вывести всех преподователей по конкретному студенту и наоборот,
по конкретному преподавателю вывести всех его студентов.
 */

import homework08.dao.TablesDAO;
import homework08.service.StudentService;
import homework08.service.TeacherService;
import homework08.utils.DBConnection;
import jakarta.persistence.EntityManager;


public class TeacherStudentMain {

    public static void main(String[] args) {

        // create connection with DB
        DBConnection connection = new DBConnection();
        EntityManager em = connection.getEm();

        // create tables with 3 teachers and students
        TablesDAO tables = new TablesDAO();
        tables.createExample(em);

        // find and print teacher 2 with his students
        TeacherService teacherService = new TeacherService(em);
        teacherService.printTeacher(2L);

        // find and print student 3 with his teachers
        StudentService studentService = new StudentService(em);
        studentService.printStudent(3L);
    }
}
