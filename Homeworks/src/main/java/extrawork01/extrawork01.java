package extrawork01;

/**
 * Программа для преобразования 4-х чисел из десятичного (10) в двоичное (2) представление и
 *  4-х чисел из двоичного (2) в десятичное (10) представление.
 *  Ввод чисел осуществляется через консоль с последующей проверкой на корректность.
 *  Результат и решение выводятся на консоль и записываются в файл.
 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class extrawork01 {

    static String allSolutions; // общий текст всех решений для вывода на консоль и записи в файл

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in); // объявление сканера

        // первое сообщение в общем тексте
        allSolutions = "Преобразование чисел из десятичного (10) в двоичное (2) представление:\n";

        System.out.println("Введите четыре числа в десятичном (10) представлении, нажимая Enter после каждого:");

        // цикл для поочередного ввода 4-х чисел (10)
        for (int i=0; i<4; i++) {

            // присвоение введенного числа переменной строки для последующей проверки на недопустимые символы
            String decString = scanner.next();

            // вызов метода для преобразования (10) -> (2) с передачей введенного числа
            decimalToBinary (decString);
        }

        // добавление к общему тексту нового сообщения
        allSolutions = allSolutions + "\n" + "Преобразование чисел из двоичного (2) в десятичное (10) представление:\n";

        System.out.println("Введите четыре числа в двоичном (2) представлении, нажимая Enter после каждого:");

        // цикл для поочередного ввода 4-х чисел (2)
        for (int i=0; i<4; i++) {

            String binString = scanner.next();

            // вызов метода для преобразования (2) -> (10)
            binaryToDecimal (binString);
        }

        System.out.print("\n");

        // вывод на консоль общего результата и решения
        System.out.println(allSolutions);

        // создание файла для записи с названием и указанием пути (без пути сохраняется в папку текущего проекта)
        File homeWorkFile = new File("HomeWork1_2.txt");

        {
            try { // блок с поиском исключений

                // создание экземпляра homeWorkFileWriter объекта FileWriter для записи файла
                FileWriter homeWorkFileWriter = new FileWriter(homeWorkFile);

                // запись общего текста всех решений в объект homeWorkFileWriter (в буфер памяти)
                homeWorkFileWriter.write(allSolutions);
                homeWorkFileWriter.flush(); // запись из буфера в файл
                homeWorkFileWriter.close(); // закрытие потока записи

            } catch (IOException e) { // отлов исключений (ошибок при записи файла)
                e.printStackTrace();
            }
        }

    }

    // метод для преобразования (10) -> (2) с передачей ранее введенных чисел (записанных в строки)
    static void decimalToBinary(String decStr) {

        // для множественных изменений в строках создаем экземпляры StringBuilder
        StringBuilder decToBin = new StringBuilder(); // для результата преобразования (10) -> (2)
        StringBuilder decSolutionStr = new StringBuilder(); // для решения преобразования (10) -> (2)
        String decSolution; // очередное значение числа (10) для решения (10) -> (2)

        // цикл для посимвольной обработки строки и проверкой на число
        for (int i=0; i<=decStr.length()-1; i++) {

            char decChar = decStr.charAt(i); // первый символ строки

            if (!Character.isDigit(decChar)) { // если символ НЕ число

                // добавление сообщения об ошибке в общий текст
                allSolutions = allSolutions + "\n" + decStr + " - не корректное число!\n";
                return; // выход из метода
            }

        }

        decSolution = decStr; // запоминание введенного числа для решения
        int decimal = Integer.parseInt(decStr); // преобразования строки в число decimal

        // цикл для поочередного деления числа на 2 и выделения остатка деления, пока число decimal больше или равно 1
        while (decimal>=1) {

            if (decimal == 1) { // если число decimal равно 1 (конец цикла)

                // вставка в результат числа decimal в начальную (offset: 0) позицию
                decToBin.insert(0, decimal);

                // присоединение к решению числа decimal и переноса строки
                decSolutionStr.append(decimal).append("\n");
                break; // выход из цикла

            } else { // если число не больше 1 (продолжение цикла)

                if (decimal % 2 != 0) { // если есть остаток от деления

                    decToBin.insert(0, "1"); // вставка в результат числа 1

                    // присоединение к решению числа decimal, формулы и переноса строки
                    decSolutionStr.append(decimal).append(" / 2 = 1\n");

                } else { // если деление без остатка

                    decToBin.insert(0, "0"); // вставка в результат числа 0

                    // присоединение к решению числа decimal, формулы и переноса строки
                    decSolutionStr.append(decimal).append(" / 2 = 0\n");

                }

                decimal = decimal / 2; // деление числа decimal на 2 и запись результата в число decimal
            }

        }

        // добавление в общий текст результата и решения
        allSolutions = allSolutions + "\n" + decSolution + "(10) -> " + decToBin + "(2)" + "\n" + decSolutionStr;

    }

    // метод для преобразования (10) -> (2) с передачей ранее введенных чисел (записанных в строки)
    static void binaryToDecimal(String binStr) {

        int binToDec = 0; // результат преобразования (2) -> (10)
        String binSolution = ""; // решение (2) -> (10)
        String binSolutionInt = ""; // дополнительное решение (2) -> (10)

        // цикл для посимвольной обработки строки (обратный счет i, начиная с последнего символа), ввод числа j для прямого счета
        for (int i=binStr.length()-1 , j=0; i>=0; i--) {

            char binChar = binStr.charAt(i); // выделение символа

            if (binChar == '0' | binChar == '1') { // если символ равен 0 или 1

                int bin = Character.getNumericValue(binChar); // преобразование символа в число bin

                // прибавление к результату произведения числа bin на 2, возведенное в степень J
                binToDec = binToDec + (int) (bin * Math.pow(2, j));

                if (j==0) { // если j=0 (первая итерация цикла)

                    // в решение записывается произведение числа bin на 2, возведенное в степень J (без символа +)
                    binSolution = bin + " * 2^" + j;

                    // к дополнительному решению добавляется результат произведения числа bin на 2, возведенного в степень J (без символа +)
                    // пустая строка "" нужна для преобразования числа результата в строку
                    binSolutionInt = "" + ((int) (bin * Math.pow(2, j)));

                } else { // если НЕ первая итерация цикла

                    // к решению добавляется произведение числа bin на 2, возведенное в степень J и символ +
                    binSolution = bin + " * 2^" + j + " + " + binSolution;

                    // к дополнительному решению добавляется результат произведения числа bin на 2, возведенного в степень J и символ +
                    binSolutionInt = ((int) (bin * Math.pow(2, j))) + " + " + binSolutionInt;
                }

                j++; // прямой счет j

            } else { // если символ НЕ равен 0 или 1

                // добавление сообщения об ошибке в общий текст
                allSolutions = allSolutions + "\n" + binStr + " - не корректное число!\n";
                return; // выход из метода
            }
        }

        // добавление в общий текст результата и решения
        allSolutions = allSolutions + "\n" + binStr + "(2) -> " + binToDec + "(10)" + "\n" + binStr + "(2) = "
                + binSolution + " = " + binSolutionInt + " = " + binToDec + "\n";

    }
}