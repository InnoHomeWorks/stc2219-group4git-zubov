package homework07;

import java.util.Objects;

public class User {
    private String name;
    private String lastName;
    private int age;
    private boolean hasWork;

    public User(String name, String lastName, int age, boolean hasWork) {

        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.hasWork = hasWork;
    }

    public User() {
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

    public void setAge(int age) {

        this.age = age;
    }

    public void setHasWork(boolean hasWork) {

        this.hasWork = hasWork;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return age == user.age && hasWork == user.hasWork && Objects.equals(name, user.name) && Objects.equals(lastName, user.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName, age, hasWork);
    }

    // add splitters for print or write
    public String toString() {

        return "|" + this.name + "|" + this.lastName + "|" + this.age + "|" + this.hasWork;
    }
}
