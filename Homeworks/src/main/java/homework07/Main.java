package homework07;
/*
### 7. Application

Реализовать в классе **UsersRepositoryFileImpl** методы:

**User findById(int id);** - поиск юзера по ID\
**void create(User user);** - создание нового юзера (запись в файл)\
**void update(User user);** - обновление информации по юзеру\
**void delete(int id);** - удаление юзера по ID

Принцип работы методов:

Пусть в файле есть запись:

**(ID|ИМЯ|ФАМИЛИЯ|ВОЗРАСТ|ЕСТЬ_РАБОТА)** <- это пояснение столбцов. Этого в файле быть не должно

**1|Иезекииль|Булыжников|33|true**\
**2|Василий|Камушкин|23|false**

> **ID** - гарантированно уникальный идентификатор пользователя (целое число). Гарантированно уникальный - у каждого пользователя свое число, которое соответствует только данному пользователю. Не должно существовать два разных юзера с одинаковыми ID\
> **ИМЯ** - имя юзера\
> **ФАМИЛИЯ** - фамилия юзера\
> **ВОЗРАСТ** - возраст юзера\
> **ЕСТЬ_РАБОТА** - статус есть работа / безработный

Тогда **findById(1)** вернет объект юзер с данными указанной строки.

Далее, для этого объекта можно выполнить следующий код:

**user.setName("Владимир");**\
**user.setAge(27);**

и выполнить **update(user)**;

При этом в файле строка будет заменена на **1|Владимир|Булыжников|27|true**

Таким образом, метод находит в файле пользователя с ID user-а и заменяет его значения.

Метод **create(User user)** принимает на вход объект юзера и записывает его в файл.

Метод **delete(int id)** удаляет из файла строку, с данными пользователя, у которого id соответствует числу, переданному в метод.

Примечания:\
Бесполезно пытаться реализовать замену данных в файле без полной перезаписи файла ;)
 */

public class Main {

    public static void main(String[] args) {

        // create Repository
        UsersRepositoryFileImpl repository = new UsersRepositoryFileImpl();

        // try to update user without first searching by ID
        repository.update(repository.user);

        // search and print user by ID = 3
        System.out.println(repository.findById(3));

        // changing the custom fields of found user
        repository.user.setLastName("Носов");
        repository.user.setAge(65);
        repository.user.setHasWork(true);

        // user update (overwriting updated fields)
        repository.update(repository.user);
        // search and print user by ID = 3 (with changed fields)
        System.out.println(repository.findById(3));

        // search and update all fields at once of user by ID = 2
        repository.findById(2);
        repository.update(new User("Незнам", "Незнайкин", 12, false));

        // delete users
        repository.delete(1);
        repository.delete(7);

        // delete non-existent user
        repository.delete(1);

        // create new user and print his ID
        repository.create(new User("Пачкуале", "Пестрини", 13, false));

        // try to create existing user
        repository.create(new User("Незнам", "Незнайкин", 12, false));

        // print all users (with keys)
        System.out.println(repository.userMap);

        // try to find and print user by non-existent ID
        System.out.println(repository.findById(10));

        // try to delete user by non-existent ID
        repository.delete(11);
    }
}
