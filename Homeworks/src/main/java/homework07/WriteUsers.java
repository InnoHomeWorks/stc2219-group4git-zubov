package homework07;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

// class to write to file
public class WriteUsers {

    private final String FILE_NAME;
    private final Map<Integer, User> userMap;

    // constructor takes filename and map
    public WriteUsers(String FILE_NAME, Map<Integer, User> userMap) {
        this.FILE_NAME = FILE_NAME;
        this.userMap = userMap;
    }

    // method for write to file from map
    public void write() {

        // new way through NIO, stream, lambda
        try {
            Path path = Paths.get(FILE_NAME);
            Files.write(path, userMap.entrySet()
                    .stream()
                    .map(k -> k.getKey() + "" + k.getValue())
                    .collect(Collectors.toList()));

        } catch (IOException e) {

            throw new RuntimeException(e);
        }

        // old way through IO BufferedWriter and loop
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_NAME))) {
//
//            // write of each map element (user print format overridden)
//            for (Map.Entry<Integer, User> element : userMap.entrySet()) {
//
//                writer.write(element.getKey().toString() + element.getValue() + "\n");
//            }
//
//        } catch (IOException e) {
//
//            throw new RuntimeException(e);
//        }
    }
}
