package homework07;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

// class for read from file
public class ReadUsers {

    private final String FILE_NAME;

    // constructor takes filename
    public ReadUsers(String FILE_NAME) {
        this.FILE_NAME = FILE_NAME;
    }

    // method for read from file and write to map
    public HashMap<Integer, User> read() {

        // create map
        HashMap<Integer, User> userMap = new HashMap<>();

        // new way through NIO, stream, lambda
        try {
            Path path = Paths.get(FILE_NAME);
            // read file in one sheet, create list of lines
            List<String> lines = Files.readAllLines(path);

            // split each line into elements and write them to map
            lines.stream().forEach(s -> {
                String[] subS = s.split("\\|");
                userMap.put(Integer.parseInt(subS[0]),
                        new User(subS[1],
                                subS[2],
                                Integer.parseInt(subS[3]),
                                Boolean.parseBoolean(subS[4])));
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // old way through IO BufferedReader and loop
//        try (BufferedReader reader = new BufferedReader(new FileReader(FILE_NAME))) {
//
//            String line;
//
//            // read line by line, split line with splitter, and write to map
//            while ((line = reader.readLine()) != null) {
//
//                String[] subLine = line.split("\\|");
//                userMap.put(Integer.parseInt(subLine[0]),
//                        new User(subLine[1],
//                                subLine[2],
//                                Integer.parseInt(subLine[3]),
//                                Boolean.parseBoolean(subLine[4])));
//            }
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }

        return userMap;
    }
}
