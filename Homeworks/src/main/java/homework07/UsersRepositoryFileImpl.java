package homework07;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UsersRepositoryFileImpl {

    private int ID; // current map key
    Map<Integer, User> userMap; // map with data from file
    User user;
    Set<Integer> deleteKeys = new HashSet<>(); // set of remote keys

    private final String FILE_NAME = "Homeworks/src/main/java/homework07/Users.txt"; // path to read/write file

    // read file and write to map
    {
        userMap = new ReadUsers(FILE_NAME).read();
    }

    // search user by ID
    public User findById(int id) {

        ID = id;

        // check to exist ID
        if (userMap.containsKey(id)) {

            user = userMap.get(id);
        } else {

            // if there is no ID in map
            System.out.println("Пользователь по ID = " + id + " не найден!");
            // reset user and ID
            user = new User();
            ID = 0;
        }
        return user;
    }

    // user overwrite
    public void update(User user) {

        // was there a search by ID
        if (ID != 0) {

            userMap.put(ID, user); // запись юзера в мап
            new WriteUsers(FILE_NAME, userMap).write(); // запись в файл из мап
            System.out.println("Данные пользователя по ID = " + ID + " успешно обновлены!");

        } else {

            System.out.println("Перед тем, как обновить, найдите пользователя по ID!");
        }
    }

    // create new user
    public void create(User user) {

        // if there is no such user in the map
        if (!userMap.containsValue(user)) {

            // create new unique ID for new user:
            // first, the maximum number from all keys is taken and incremented by 1
            int newID = Collections.max(userMap.keySet()) + 1;

            // then it is checked if such key previously existed (was deleted with user)
            while (deleteKeys.contains(newID)) {

                newID++; // then the new key is incremented by 1
            }

            userMap.put(newID, user); // write to map new user by new ID
            System.out.println("Пользователь " + user + " успешно добавлен в репозиторий под ID = " + newID);
            new WriteUsers(FILE_NAME, userMap).write(); // write to file from map

            // if there is already such user
        } else {

            // search ID of found user:

            // new way through stream, lambda
            userMap.entrySet()
                    .stream()
                    .forEach(v -> {
                        if (user.equals(v.getValue())) {
                            System.out.println("Пользователь " + user +
                                    " уже есть в репозитории под ID = " + v.getKey() + "!");
                        }
                    });

            // old way through loop
//            for (Map.Entry<Integer, User> element : userMap.entrySet()) {
//
//                if(user.equals(element.getValue())) {
//
//                    System.out.println("Пользователь " + user +
//                            " уже есть в репозитории под ID = " + element.getKey() + "!");
//                }
//            }
        }
    }

    // remove user by ID
    public void delete(int id) {

        // check for ID content in map
        if (userMap.containsKey(id)) {

            deleteKeys.add(id); // add ID to set of remote keys
            userMap.remove(id); // remove from map
            new WriteUsers(FILE_NAME, userMap).write(); // write file from map
            System.out.println("Пользователь по ID = " + id + " успешно удален!");

        } else {

            // if there is no ID in map
            System.out.println("Пользователь по ID = " + id + " не найден!");
        }
    }
}
