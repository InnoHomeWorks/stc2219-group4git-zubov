/**
 * Extrawork03 with two additional methods for Homework03_2
 */

package extrawork03;

/*
Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:

> Было:
> 34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
>
> Стало после применения процедуры:
> 34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
*/

import java.util.Arrays;

public class Extrawork03 {

    public static void main(String[] args) {

        int[] array1 = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        System.out.println(Arrays.toString(array1));

        nonZeroToLeft3(array1);

        System.out.println(Arrays.toString(array1));

        nonZeroToLeft4(array1);

        System.out.println(Arrays.toString(array1));
    }

    // method №3 - with one loop and additional counter
    static void nonZeroToLeft3(int[] array) {

        int j = 0;

        for (int i=0; i<array.length - 1; i++) {

            if (array[i] == 0) {

                array[i] = array[i+1];
                array[i+1] = 0;
            }

            if(i == array.length - 2 & j != array.length - 2) {
                i = 0;
                j++;
            }
        }
    }

    // method №4 - without loops, using recurse method
    static void nonZeroToLeft4(int[] array) {

        int i = 1;
        int j = 0;
        nonZeroToLeft4Recurse(array, i, j);
    }

    static void nonZeroToLeft4Recurse(int[] array, int i, int j) {

        if (i < array.length && j < array.length) {

            if (array[j] == 0) {
                array[j] = array[i];
                array[i] = 0;
                i++;

            } else {

                i = j+1;
                j++;
            }

            nonZeroToLeft4Recurse(array, i, j);
        }
    }
}