package extrawork04;

public class Dog extends Animal{

    // declaration a counter variable for dogs
    private static int dogCount;

    {
        dogCount++;
    }

    public Dog(String name){

        super(name);
    }

    // overriding of animal's methods
    @Override
    public void running(int distance){

        System.out.print("Пёс ");

        if(distance <= 500){

            super.running(distance); // invokes non-overriding method of Animal
        } else {

            System.out.println(getName() + " пробежал 500м. и устал, дальше бежать не может!");
        }
    }

    @Override
    public void swimming(int distance){

        System.out.print("Пёс ");

        if(distance <= 10){

            super.swimming(distance);
        } else {

            System.out.println(getName() + " проплыл 10м., дальше побоялся и поплыл обратно.");
        }
    }

    public static int getDogCount() {

        return dogCount;
    }
}
