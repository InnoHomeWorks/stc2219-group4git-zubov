package extrawork04;

/*
1. Создать классы Собака и Кот с наследованием от класса Животное.
2. Все животные могут бежать и плыть. В качестве параметра каждому методу передается длина
препятствия. Результатом выполнения действия будет печать в консоль. (Например,
dogBobik.run(150); -> 'Бобик пробежал 150 м.');
3. У каждого животного есть ограничения на действия (бег: кот 200 м., собака 500 м.; плавание:
кот не умеет плавать, собака 10 м.).
4. * Добавить подсчет созданных котов, собак и животных.
 */

public class Extrawork04 {

    public static void main(String[] args) {

        // creation of cats and dogs
        Cat cat1 = new Cat("Барсик");
        Cat cat2 = new Cat("Мурзик");
        Cat cat3 = new Cat("Васька");
        Cat cat4 = new Cat("Пушок");
        Dog dog1 = new Dog("Полкан");
        Dog dog2 = new Dog("Мухтар");
        Dog dog3 = new Dog("Дружок");

        // invokes methods for cats and dogs with distance
        cat1.running(100);
        cat1.swimming(10);
        System.out.println();

        cat2.running(300);
        cat2.swimming(10);
        System.out.println();

        cat3.running(250);
        cat3.swimming(10);
        System.out.println();

        cat4.running(50);
        cat4.swimming(10);
        System.out.println();

        dog1.running(250);
        dog1.swimming(9);
        System.out.println();

        dog2.running(600);
        dog2.swimming(9);
        System.out.println();

        dog3.running(300);
        dog3.swimming(20);
        System.out.println();

        // invokes methods through array and loop

        Animal[] animals = {cat1, cat2, cat3, cat4, dog1, dog2, dog3};

        for (Animal animal : animals) {

            animal.running(100);
            animal.swimming(9);
            System.out.println();
        }

        for (Animal animal : animals) {

            animal.running(600);
            animal.swimming(20);
            System.out.println();
        }

        // print counts
        System.out.println("Котов было " + Cat.getCatCount());
        System.out.println("Собак было " + Dog.getDogCount());
        System.out.println("Животных было всего " + Animal.getAnimalCount());
    }
}
