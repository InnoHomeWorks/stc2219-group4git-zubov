package extrawork04;

public class Cat extends Animal{

    // declaration a counter variable for cats
    private static int catCount;

    {
        catCount++;
    }
    public Cat(String name){

        super(name);
    }

    // overriding of animal's methods
    @Override
    public void running(int distance){

        System.out.print("Кот ");

        if(distance <= 200){

            super.running(distance); // invokes non-overriding method of Animal
        } else {

            System.out.println(getName() + " пробежал 200м. и устал, дальше бежать не может!");
        }
    }

    @Override
    public void swimming(int distance){

        System.out.println("Кот " + getName() + " отказался плыть. Коты не умеют плавать! (или не хотят)");
    }

    public static int getCatCount() {

        return catCount;
    }
}