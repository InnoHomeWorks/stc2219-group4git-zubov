package extrawork04;

public abstract class Animal {

    private final String name;

    // declaration a counter variable for all animals
    private static int animalCount;

    // counts on each creation
    {
        animalCount++;
    }

    public Animal(String name){

        this.name = name;
    }
    public String getName() {

        return name;
    }

    // common methods for all animals
    public void running(int distance){

        System.out.println(name + " пробежал " + distance + "м.");
    }

    public void swimming(int distance){

        System.out.println(name + " проплыл " + distance + "м.");
    }

    // returns counting result
    public static int getAnimalCount(){

        return animalCount;
    }
}

