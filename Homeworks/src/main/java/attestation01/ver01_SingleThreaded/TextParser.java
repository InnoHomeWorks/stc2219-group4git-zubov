package attestation01.ver01_SingleThreaded;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// класс для обработки списка строк
public class TextParser {

    private final List<String> lines;

    // конструктор принимает список строк
    public TextParser(List<String> lines) {
        this.lines = lines;
    }

    // метод для подсчета слов с записью результата в мапу и ее возвратом
    public Map<String, Integer> wordParsing() {

        Map<String, Integer> resultMap = new HashMap<>();

        lines.stream()
                .map(line -> line.replaceAll("[^а-яёА-ЯЁ -]+", "")) // исключение символов, кроме указанных
                .map(String::toLowerCase) // приведение к нижнему регистру
                .map(line -> line.split(" ")) // разделение строки на массив слов
                .forEach(words -> Arrays.stream(words) // преобразование массива в стрим
                        .forEach(word -> { // и работа с каждым словом

                            if (!(word.equals("") | word.equals("-"))) { // исключение слов, состоящих из пустого символа и тире

                                if (!resultMap.containsKey(word)) { // если мапа не содержит ключ из слова

                                    resultMap.put(word, 1); // то по ключу кладется слово, по значению - 1
                                } else {
                                    resultMap.put(word, resultMap.get(word) + 1); // иначе к значению прибавляется 1
                                }
                            }
                        }));

        return resultMap;
    }

    // метод для подсчета букв с записью результата в мапу и ее возвратом
    public Map<String, Integer> letterParsing() {

        Map<String, Integer> resultMap = new HashMap<>();

        lines.stream()
                .map(line -> line.replaceAll("[^а-яёА-ЯЁ]+", "")) // исключение символов, кроме указанных
                .map(String::toLowerCase) // приведение к нижнему регистру
                .map(line -> line.split("")) // разделение строки на массив букв
                .forEach(letters -> Arrays.stream(letters) // преобразование массива в стрим
                        .forEach(letter -> { // и работа с каждой буквой

                            if (!letter.equals("")) { // исключение букв, состоящих из пустого символа

                                if (!resultMap.containsKey(letter)) { // если мапа не содержит ключ из буквы

                                    resultMap.put(letter, 1); // то по ключу кладется буква, по значению - 1
                                } else {
                                    resultMap.put(letter, resultMap.get(letter) + 1); // иначе к значению прибавляется 1
                                }
                            }
                        }));
        return resultMap;
    }
}
