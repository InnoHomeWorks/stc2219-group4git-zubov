package attestation01.ver01_SingleThreaded;

public class Main {

    public static void main(String[] args) {

        TextCount textCount = new TextCount(); // запуск общего класса обработчика

        // запуск отдельных методов для обработки по словам и по буквам
        textCount.wordsParseAndWrite();
        textCount.lettersParseAndWrite();
    }
}
