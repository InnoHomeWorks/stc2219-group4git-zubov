package attestation01.ver01_SingleThreaded;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

// класс для чтения файла
public class ReadFile {

    private final Path path;

    // конструктор принимает путь файла-источника
    public ReadFile(Path path) {
        this.path = path;
    }

    // метод считывает текст из файла и возвращает в виде списка строк
    public List<String> readLines() {

        List<String> lines;

        try {
            lines = Files.readAllLines(path);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }
}
