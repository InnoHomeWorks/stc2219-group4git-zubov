package attestation01.ver01_SingleThreaded;

import java.nio.file.Path;

// общий класс обработчика для активации объектов сканера, чтения из файла, обработки и записи в файлы
public class TextCount {
    private final Path path;
    private final String wordsFileName = "wordCount.txt"; // название нового файла с результатом подсчета слов
    private final String lettersFileName = "letterCount.txt"; // название нового файла с результатом подсчета букв

    // получение пути файла-источника от пользователя
    ScannerPath scannerPath = new ScannerPath();

    {
        scannerPath.askPath();
        path = scannerPath.getPath();
    }

    // чтение из файла-источника списка строк и передача их в объект текстового обработчика
    ReadFile readFile = new ReadFile(path);
    TextParser textParser = new TextParser(readFile.readLines());

    // методы для запуска обработки и записи результатов в файлы
    public void wordsParseAndWrite() {

        System.out.print("Файл с подсчетом слов находится по адресу: ");
        WriteFile writeFile = new WriteFile(path, wordsFileName, textParser.wordParsing());
        writeFile.writeMap();
    }

    public void lettersParseAndWrite() {

        System.out.print("Файл с подсчетом букв находится по адресу: ");
        WriteFile writeFile = new WriteFile(path, lettersFileName, textParser.letterParsing());
        writeFile.writeMap();
    }
}
