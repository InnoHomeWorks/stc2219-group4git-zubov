package attestation01.ver01_SingleThreaded;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Collator;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;

// класс для записи результата обработки в файл
public class WriteFile {

    private Path path; // путь файла-источника
    private final String newFileName; // название нового файла для записи
    private final Map<String,Integer> resultMap; // мапа с результатом обработки

    public WriteFile(Path path, String newFileName, Map<String, Integer> resultMap) {
        this.path = path;
        this.newFileName = newFileName;
        this.resultMap = resultMap;
    }

    // метод для записи мапы в новый файл
    public void writeMap() {

        // если родительский каталог - С: (куда ограничен доступ для сохранения новых файлов)
        if(path.getParent().equals(Paths.get("C:\\"))) {

            path = path.getFileName(); // то из пути выделяется имя файла (тип путь)
        }

        // если путь файла состоит только из названия файла
        Path newFilePath = path.getParent() == null ?
                Paths.get(newFileName) : // то путь для нового файла создается по умолчанию
                Paths.get(path.getParent() + "/" + newFileName); // иначе новый файл создается в каталоге файла-источника

        // для более корректной сортировки результата по алфавиту (с учетом буквы ё) используется локаль с настройками языка
        Collator collator = Collator.getInstance(new java.util.Locale("ru", "RU"));

        try {

            // если нового файла не существует (первый запуск программы), создается новый файл
            Path newFile = Files.notExists(newFilePath) ? Files.createFile(newFilePath) : newFilePath;

            // запись (перезапись) в новый файл ключа и значения мапы с разделителем
            Files.write(newFile, resultMap.entrySet()
                    .stream()
                    .map(k -> k.getKey() + " - " + k.getValue())
                    .sorted(Comparator.comparing(s -> s, collator))
                    .collect(Collectors.toList()));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println(newFilePath.toAbsolutePath()); // вывод на консоль абсолютного пути нового файла
    }
}
