package attestation01.ver02_MultiThreaded_Modify;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// класс для обработки списка строк
public class TextParser {

    private final List<String> lines;

    // конструктор принимает список строк
    public TextParser(List<String> lines) {
        this.lines = lines;
    }

    // метод для подсчета слов с записью результата в мапу и ее возвратом
    public Map<String, Integer> wordParsing() {

        Map<String, Integer> resultMap = new HashMap<>(); // мапа с результатами обработки

        lines.parallelStream() // создание параллельных потоков
                .map(String::toLowerCase) // приведение к нижнему регистру
                .map(line -> line.replaceAll("[^а-яё -]+", "")) // исключение символов, кроме указанных
                .map(line -> line.split(" ")) // разделение строки на массив слов
                .forEach(words -> Arrays.stream(words) // преобразование массива в стрим
                        .filter(word -> !(word.equals("") | word.equals("-"))) // исключение слов, состоящих из пустого символа и тире
                        .forEach(word -> { // и работа с каждым словом

                            // синхронизация по мапе для исключения одновременной записи нескольких потоков
                            synchronized (resultMap) {

                                if (!resultMap.containsKey(word)) { // если мапа не содержит ключ из слова

                                    resultMap.put(word, 1); // то по ключу кладется слово, по значению - 1
                                } else {
                                    resultMap.put(word, resultMap.get(word) + 1); // иначе к значению прибавляется 1
                                }
                            }
                        }));

        return resultMap;
    }

    // метод для подсчета букв с записью результата в мапу и ее возвратом
    public Map<String, Integer> letterParsing() {

        Map<String, Integer> resultMap = new HashMap<>();

        lines.parallelStream() // создание параллельных потоков
                .map(String::toLowerCase) // приведение к нижнему регистру
                .map(line -> line.replaceAll("[^а-яё]+", "")) // исключение символов, кроме указанных
                .map(line -> line.split("")) // разделение строки на массив букв
                .forEach(letters -> Arrays.stream(letters) // преобразование массива в стрим
                        .filter(word -> !word.equals("")) // исключение букв, состоящих из пустого символа
                        .forEach(letter -> { // и работа с каждой буквой

                            // синхронизация по мапе для исключения одновременной записи нескольких потоков
                            synchronized (resultMap) {

                                if (!resultMap.containsKey(letter)) { // если мапа не содержит ключ из буквы

                                    resultMap.put(letter, 1); // то по ключу кладется буква, по значению - 1
                                } else {
                                    resultMap.put(letter, resultMap.get(letter) + 1); // иначе к значению прибавляется 1
                                }
                            }
                        }));
        return resultMap;
    }
}
