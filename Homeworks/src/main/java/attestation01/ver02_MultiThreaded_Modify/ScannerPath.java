package attestation01.ver02_MultiThreaded_Modify;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

// класс для получения от пользователя пути к файлу с проверками
public class ScannerPath {

    private Path path;

    public Path getPath() {
        return path;
    }

    public void askPath() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите путь к файлу, в котором находится текст,");
        System.out.println("например: Homeworks/src/main/java/attestation01/ver02_MultiThreaded_Modify/text.txt");
        System.out.println("или: D:/text.txt");

        // проверка на пустую строку при вводе данных
        while (scanner.hasNext()) {

            String fileName = scanner.nextLine();
            File tempFile = new File(fileName);

            // проверка, является ли файлом введенный путь
            if (tempFile.isFile()) {

                path = Paths.get(fileName);
                return;
            }
            System.out.println("Файл не найден! Введите корректный путь к файлу.");
        }

    }
}
