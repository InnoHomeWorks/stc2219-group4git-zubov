package attestation01.ver02_MultiThreaded_Modify;

import java.nio.file.Path;

// общий класс обработчика для активации объектов сканера, чтения из файла, обработки и записи в файлы
public class TextCount {
    private final Path path; // путь файла-источника
    private final String wordsFileName = "wordCount.txt"; // название нового файла с результатом подсчета слов
    private final String lettersFileName = "letterCount.txt"; // название нового файла с результатом подсчета букв

    // получение пути файла-источника от пользователя
    ScannerPath scannerPath = new ScannerPath();

    {
        scannerPath.askPath();
        path = scannerPath.getPath();
    }

    // создание объектов для чтения из файла-источника списка строк,
    // для передачи их в объект текстового обработчика
    // и для записи в файл
    ReadFile readFile = new ReadFile(path);
    TextParser textParser = new TextParser(readFile.readLines());
    WriteFile writeFile = new WriteFile(path);

    // потоки для обработки и записи результатов
    Thread tread1 = new Thread(() -> {

        writeFile.writeMap(wordsFileName, textParser.wordParsing());
    });

    Thread tread2 = new Thread(() -> {

        writeFile.writeMap(lettersFileName, textParser.letterParsing());
    });

    public static void main(String[] args) {

        // создание объекта обработчика
        TextCount textCount = new TextCount();

        // запуск потоков
        textCount.tread1.start();
        textCount.tread2.start();

    }
}
