package homework06;

/*
6. Лямбда выражения

Предусмотреть функциональный интерфейс:

> interface ByCondition {
>
>     boolean isOk(int number);
>
> }

Написать класс **Sequence**, в котором должен присутствовать метод **filter**:

> public static int[] filter(int[] array, ByCondition condition) {
>
>   ...
>
> }

Данный метод возвращает массив, который содержит элементы, удовлетворяющие логическому выражению в **condition**.

В main в качестве **condition** подставить:

- проверку на четность элемента
- проверку, является ли сумма цифр элемента четным числом.
- Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
- Доп. задача: проверка на палиндромность числа (число читается одинаково и слева, и справа ->
11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).
 */

import java.util.Arrays;

public class LambdaMain {

    static int[] array = {-33, 0, 444, 8, 1563, 0, 14, 556, 24, 0, -3, 0, 55, -4, 12321, 57, 3333, 2332, 12344321};


    public static void main(String[] args) {

        System.out.println("Первоначальный массив:");
        System.out.println(Arrays.toString(array));

        System.out.println("\nМассив с четными элементами:");
        System.out.println(Arrays.toString(Sequence.filter(array, x -> x % 2 == 0)));

        System.out.println("\nМассив, где каждый элемент представлен в виде четной суммы цифр:");
        System.out.println(Arrays.toString(Sequence.filter(array, x -> {

            int sum = 0;

            for(int j : elementToArray(x)) {

                sum += j;
            }
            return sum % 2 == 0;
        })));

        System.out.println("\nМассив, где каждый элемент состоит из четных цифр:");
        System.out.println(Arrays.toString(Sequence.filter(array, x -> {
            for (int element : elementToArray(x)) {
                if (element % 2 != 0) return false;
            }
            return true;
        })));

        // alternative way through equals
        System.out.println("\nМассив, где каждый элемент состоит из четных цифр (альтернативный способ):");
        System.out.println(Arrays.toString(Sequence.filter(array, x ->

                Arrays.equals(elementToArray(x), Sequence.filter(elementToArray(x), x1 -> x1 % 2 == 0))
        )));

        System.out.println("\nМассив с элементами-палиндромами:");
        System.out.println(Arrays.toString(Sequence.filter(array, x -> {

            if(x < 0) return false; // negative numbers cannot be palindromes

            int[] reverseElement = new int[elementToArray(x).length];
            int j = 0;

            for(int i = elementToArray(x).length - 1; i >= 0; i--) {

                reverseElement[j] = elementToArray(x)[i];
                j++;
            }
            return Arrays.equals(reverseElement, elementToArray(x));
        })));

        // alternative way through StringBuilder (using method reverse)
        System.out.println("\nМассив с элементами-палиндромами (альтернативный способ):");
        System.out.println(Arrays.toString(Sequence.filter(array, x -> {

            if(x < 0) return false; // negative numbers cannot be palindromes

            return x == Integer.parseInt((new StringBuilder(String.valueOf(x))).reverse().toString());
        })));

    }

    // method for create new array of numbers included in element of array
    static int[] elementToArray(int element) {

        // find length for new array
        int length = 0;

        if (element == 0) length = 1;

        for (int i = 1; i <= Math.abs(element); i *= 10) {

            length++;
        }

        // alternative way to find length through String
//        int length = String.valueOf(element).length();

        int[] newArray = new int[length];

        for (int i = length - 1; i >= 0; i--) {

            newArray[i] = element % 10;
            element /= 10;
        }

        return newArray;
    }
}
