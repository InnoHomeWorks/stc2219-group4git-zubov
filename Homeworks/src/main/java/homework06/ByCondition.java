package homework06;

@FunctionalInterface

public interface ByCondition {

    boolean isOk(int number);
}