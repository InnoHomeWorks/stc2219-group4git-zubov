package homework06;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {

        // find length for new array
        int length = 0;

        for (int element : array) {

            if (condition.isOk(element)) {

                length++;
            }
        }

        // create new array
        int[] newArray = new int[length];

        int k = 0;

        for (int j = 0; j < newArray.length; j++) {

            for (int i = k; i < array.length; i++) {

                if (condition.isOk(array[i])) {

                    newArray[j] = array[i];
                    k = i + 1;
                    break;
                }
            }
        }
        return newArray;
    }
}