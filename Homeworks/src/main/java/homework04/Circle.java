package homework04;

public class Circle extends Ellipse implements Movable {

    public Circle(int x, int y, int x1, int y1) { // circle has two points

        super(x , y, x1, y1, x1, y1);
    }

    public void move(int x, int y) {

        setX(getX() + x);
        setY(getY() + y);
        setX1(getX1() + x);
        setY1(getY1() + y);
        setX2(getX1());
        setY2(getY1());
    }

    @Override
    public String toString() {

        return "Circle: Coordinates " + "(" + getX() + ", " + getY() + ", " + getX1() + ", " + getY1() + "); ";
    }
}

