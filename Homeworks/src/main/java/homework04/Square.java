package homework04;

public class Square extends Rectangle implements Movable {

    public Square(int x, int y, int x1, int y1) { // square has two points

        super(x, y, x1, y1, x1, y1);
    }

    // method for move all points of figure on value x and y
    public void move(int x, int y) {

        setX(getX() + x);
        setY(getY() + y);
        setX1(getX1() + x);
        setY1(getY1() + y);
        setX2(getX1());
        setY2(getY1());
    }

    @Override
    public String toString() {

        return "Square: Coordinates " + "(" + getX() + ", " + getY() + ", " + getX1() + ", " + getY1() + "); ";
    }
}

