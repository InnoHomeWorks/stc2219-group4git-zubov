package homework04;

public class Ellipse extends Figure {

    private int x1, y1, x2, y2;
    public Ellipse(int x, int y, int x1, int y1, int x2, int y2) { // ellipse has three points

        super(x, y); // firs point of ellipse (point of start of semi-axis)

        // points of end of semi-axis
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }
    // overriding method for calculating perimeter
    @Override
    public int getPerimeter() {

        // semi-axis of the Ellipse a, b
        double a = Math.sqrt((Math.pow((Math.abs(getX1() - getX())) , 2) + (Math.pow((Math.abs(getY1() - getY())) , 2))));
        double b = Math.sqrt((Math.pow((Math.abs(getX2() - getX())) , 2) + (Math.pow((Math.abs(getY2() - getY())) , 2))));

        double perimeter = 4 * (Math.PI * a * b + Math.pow(Math.abs((a - b)), 2)) / (a + b);

        return (int) Math.round(perimeter);
    }

    // overriding method for print figure with args
    @Override
    public String toString() {

        return "Ellipse: Coordinates " + "(" + getX() + ", " + getY() + ", " + getX1() + ", " + getY1() +
                getX2() + ", " + getY2() + "); ";
    }
}

