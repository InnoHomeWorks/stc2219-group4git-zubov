package homework04;
/*
Сделать абстрактный класс Figure, у данного класса есть два поля - x и y координаты.
Классы Ellipse и Rectangle должны быть потомками класса Figure.
Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
В классе Figure предусмотреть абстрактный метод getPerimeter().
Так же, нужно определить интерфейс Movable c единственным методом .move(int x, int y),
который позволит перемещать фигуру на заданные координаты.
Данный интерфейс должны реализовать только классы Circle и Square.
В Main создать массив всех фигур и "перемещаемых" фигур. У всех вывести в консоль периметр,
а у "перемещаемых" фигур изменить случайным образом координаты.
 */

import java.util.Random;

public class Homework04 {

    public static void main(String[] args) {

        // creation of figures with 2D coordinates (x, y)
        Ellipse ellipse = new Ellipse(2, 1, 5, 3, 7, -2);
        Rectangle rectangle = new Rectangle(-4, -2, 1, 3, -2, -4);
        Square square = new Square(4, 4, 3, 8);
        Circle circle = new Circle(-2, -2, -4, -5);

        // creation arrays of all figures and for Movable figures
        Figure[] figures = {ellipse, rectangle, square, circle};
        Movable[] movableFigures = {square, circle};

        // creation a loop to print all Figures and their perimeter
        for (Figure figure : figures){

            System.out.println(figure + "Perimeter = " + figure.getPerimeter());
        }

        System.out.println();

        Random random = new Random();

        // creation a loop to print movable figures with move
        for (Movable figure : movableFigures) {

            // initialization random number (in bounds from -20 to 20) for move
            int x = random.nextInt(40) - 20;
            int y = random.nextInt(40) - 20;

            // invoke method move
            figure.move(x, y);
            System.out.println("After move on values x = " + x + ", y = " + y + ": " + figure);
        }
    }
}
