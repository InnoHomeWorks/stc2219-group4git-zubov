package homework03;

/**
 * Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
 *
 * > Было:
 * > 34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
 * >
 * > Стало после применения процедуры:
 * > 34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
 */

import java.util.Arrays;

public class homework03_2 {

    public static void main(String[] args) {

        int[] array1 = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        int[] array2 = {12, 1, 3, 14, 15, 7}; // array without zeros

        int[] array3 = {0, 0, 0, 0}; // array without zeros

        // invokes method (procedure) with different arrays
        nonZeroToLeft1(array1);
        nonZeroToLeft1(array2);
        nonZeroToLeft1(array3);

        // prints arrays after procedure
        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
        System.out.println(Arrays.toString(array3));

        int[] array4 = {0, 0, 0, 345, 16, 10, 0, 0, 0, 0, 13, 0};

        // alternative method
        nonZeroToLeft2(array4);

        // prints array after alternative procedure
        System.out.println(Arrays.toString(array4));

    }

    // method for find and replaced non-zero number on left
    static void nonZeroToLeft1 (int[] array) {

        // first loop to check all elements with second loop
        for (int i=0; i<array.length-1; i++) {

            // second loop to swap neighbor elements step-by-step
            // starts by iteration of first loop to skip checked elements
            for (int j=i; j<array.length; j++) {

                if (array[i] == 0) { // if current element of array equal zero

                    array[i] = array[j]; // current element equals next element
                    array[j] = 0; // next element equals zero
                }
            }
        }
    }

    // alternative method
    static void nonZeroToLeft2 (int[] array) {

        // loop to check all elements
        for (int i=0, j=0; i<array.length; i++) {

            if(array[i]!=0) { // if current element of array is not equal zero

                array[j] = array[i]; // new element equals current element
                j++; // starts count j
            }

            if(i==array.length-1) { // if last iteration (non-zero numbers ran out)

                while (j<array.length) { // loop to fill remaining elements with zeros

                    array[j] = 0;
                    j++;
                }
            }
        }
    }
}