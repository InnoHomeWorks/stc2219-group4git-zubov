package homework03;

/**
 * Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть
 * индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.
 */

public class homework03_1 {

    public static void main(String[] args) {

        int number = 5;
        int [] array1 = {2, 4, 0, 7, 5, 33}; // first array, including number
        int [] array2 = {1, 0, 21, 3, 8, 15, 100}; // second array without number

        System.out.println(getIndexOfArray(array1, number)); // invokes method, with pass first array and number
        System.out.println(getIndexOfArray(array2, number)); // invokes method, with pass second array and number
    }

    static int getIndexOfArray(int[] array, int number) { // method (function) for find number in array

        int index = -1; // in case, when number isn't found in array

        for (int i=0; i<array.length; i++) { //

            if (number == array[i]) { // if number is found in array

                index = i;
            }
        }

        return index;
    }
}